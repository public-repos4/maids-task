import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoaderComponent} from './components/layouts/loader/loader.component';

import {LoaderInterceptor} from './interceptors/loader-interceptor';
import {LoaderService} from './services/loader.service';

import {CachingInterceptor} from "./interceptors/caching-interceptor";
import { CacheService } from './services/cache.service';

@NgModule({
  declarations: [
    AppComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,

    NgbModule,
    PaginationModule.forRoot(),
    BrowserAnimationsModule,

    AppRoutingModule
  ],
  providers: [
    LoaderService,
    {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true},

    CacheService,
    { provide: HTTP_INTERCEPTORS, useClass: CachingInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
