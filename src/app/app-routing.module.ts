import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {HomeComponent} from './components/pages/home/home.component';
import {ErrorComponent} from './components/pages/error/error.component';

const routes: Routes = [
  // {path: '', component: HomeComponent},
  {path: '', loadChildren: () => import('./components/pages/users/users.module').then(x => x.UsersModule)},
  {path: 'users', loadChildren: () => import('./components/pages/users/users.module').then(x => x.UsersModule)},
  {path: '**', component: ErrorComponent} // This line will remain down from the whole pages component list
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
