import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

import {ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { NgxNavbarModule } from 'ngx-bootstrap-navbar';
// import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {UsersRoutingModule} from './users-routing.module';
import {LayoutComponent} from './layout/layout.component';
import {ListComponent} from './list/list.component';
import {AddEditComponent} from './add_edit/add-edit.component';

// import {HttpCacheService} from '../../../services/cache.service';

@NgModule({
  declarations: [
    LayoutComponent,
    ListComponent,
    AddEditComponent
  ],
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    NgbModule,
    PaginationModule.forRoot(),

    // BrowserAnimationsModule,
    NgxNavbarModule,

    UsersRoutingModule
  ],
  providers: [],
})
export class UsersModule {
}
