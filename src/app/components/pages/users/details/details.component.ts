import {Component, OnInit} from '@angular/core';
import {first, shareReplay} from 'rxjs/operators';

import {UserService} from '../../../../services/users.service'

import {User} from '../../../../models/user';

@Component({
  selector: 'user-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})

export class DetailsComponent implements OnInit {
  user?: User;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    const url = window.location.href;
    const userId = <string>(url.split('/').pop());
    console.log(url);
    console.log(url.split('/'));
    console.log(userId);

    this.userService.getById(userId)
      .pipe(first())
      .subscribe({
        next: (response: any) => {
          this.user = response.data;
          console.log(response.data)

        },
        error: () => {
          console.log(this)
        },    // errorHandler
      });
  }
}
