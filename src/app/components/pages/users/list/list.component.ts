import {Component, OnInit} from '@angular/core';
import {PageChangedEvent} from 'ngx-bootstrap/pagination';

import {first} from 'rxjs/operators';

import {UserService} from '../../../../services/users.service'

import {User} from '../../../../models/user';

@Component({
  selector: 'users-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

export class ListComponent implements OnInit {
  foundUser = true;
  searchingUser = false;
  searchedUser?: User;

  users?: User[];
  pagedUsers?: User[];
  cachedUsers?: {};
  PerPage = 4;
  pageNumber = 1;
  pageSize = 4;
  totalItems = 10;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.getAll(this.pageNumber, this.pageSize)
      .pipe(first())
      .subscribe({
        next: (response: any) => {
          this.users = response.data;
          this.totalItems = response.total;
          this.pagedUsers = this.users;
        },
        error: () => {
          console.log(this)
        },    // errorHandler
      });
  }

  searchUsersById(e: any) {
    const userId = e.target.value;

    if (!this.isEmpty(userId) && e.key === 'Enter') {
      this.userService.getById(userId)
        .pipe()
        .subscribe({
          next: (response: any) => {
            this.searchingUser = true;
            this.foundUser = true;
            this.searchedUser = response.data;
          },
          error: () => {
            this.searchingUser = true;
            this.foundUser = false;
          },    // errorHandler
        });
    } else {
      this.searchingUser = false;
    }
  }

  setPage(event: PageChangedEvent): void {
    this.pageNumber = event.page;

    this.userService.getAll(this.pageNumber, this.pageSize)
      .pipe()
      .subscribe({
        next: (response: any) => {
          this.users = response.data;
          this.totalItems = response.total;
          this.pagedUsers = this.users;
        },
        error: () => {
          console.log(this)
        },    // errorHandler
      });
  }

  isEmpty = function (text: string): boolean {
    return text === null || text.match(/^ *$/) !== null;
  };
}
