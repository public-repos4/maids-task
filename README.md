# Maids task
Users CRUD front end application using Angular framework

## Getting started
After you download or clone the repo do the following:
Running the Angular App:
- Install NodeJS and NPM from https://nodejs.org.
- Install the Angular CLI using the command: `npm install -g @angular/cli`
- open the Application folder using VsCode from the terminal:
    ==> run `npm install` to install all required packages
    ==> run `ng serve --open` this will compile the Angular app and automatically launch it in your default browser on the URL http://localhost:4200.

