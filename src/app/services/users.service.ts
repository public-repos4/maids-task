import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {environment} from '../../environments/environment';
import {User} from '../models/user';

@Injectable({providedIn: 'root'})
export class UserService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.userSubject = new BehaviorSubject<User>(JSON.parse(<string>localStorage.getItem('user')));
    this.user = this.userSubject.asObservable();
  }

  public get userValue(): User {
    return this.userSubject.value;
  }

  // getAll(pageNumber: number, pageSize: number): Observable<User[]>{
  //   const url = `${environment.apiUrl}/users?page=` + pageNumber + `&per_page=` + pageSize;
  //   // if (cachedRequests[url] != null) return cachedRequests[url];
  //   return this.http.get<User[]>(url)
  // }

  getAll(pageNumber: number, pageSize: number) {
    return this.http.get<User[]>(`${environment.apiUrl}/users?page=${pageNumber}&per_page=${pageSize}`)
  }

  getById(id: string) {
    // return this.http.get<User>(`${environment.apiUrl}/users?id=${id}`);
    return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
  }
}
